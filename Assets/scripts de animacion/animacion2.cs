﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animacion2 : MonoBehaviour
{
    public Animator laPuerta2;

    private void OnTriggerEnter(Collider other)
    {

        laPuerta2.Play("abrir armario");
        }
    
    private void OnTriggerExit(Collider other)
    {

        laPuerta2.Play("cerrar armario");
        }
}