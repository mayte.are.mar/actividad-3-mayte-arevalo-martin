﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animacion4 : MonoBehaviour
{
    public Animator laPuerta4;

    private void OnTriggerEnter(Collider other)
    {

        laPuerta4.Play("puerta ultima");
        }
    
    private void OnTriggerExit(Collider other)
    {

        laPuerta4.Play("cerrar puerta ultima");
        }
}
