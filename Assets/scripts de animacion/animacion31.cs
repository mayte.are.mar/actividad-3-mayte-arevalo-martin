﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animacion31 : MonoBehaviour
{
    public Animator laPuerta3;

    private void OnTriggerEnter(Collider other)
    {

        laPuerta3.Play("door");
        }
    
    private void OnTriggerExit(Collider other)
    {

        laPuerta3.Play("close");
        }
}
