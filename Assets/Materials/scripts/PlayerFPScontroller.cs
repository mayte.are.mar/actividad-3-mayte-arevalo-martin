using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
[RequireComponent(typeof(CharacterMovement))]
[RequireComponent(typeof(MouseLook))]
public class PlayerFPScontroller : MonoBehaviour
{
   
   private CharacterMovement characterMovement;
   private MouseLook mouseLook;
    
    // Use this for initialization
    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);

        characterMovement = GetComponent<CharacterMovement>();
        mouseLook = GetComponent<MouseLook>();

      } 
      
    private void Update()
    {
        movement();
        rotation();
       
    }

    private void movement(){

        // Codigo de movimiento
        float hMovementInput = Input.GetAxisRaw("Horizontal") ;
       float vMovementInput = Input.GetAxisRaw("Vertical") ;

       bool jumpInput = Input.GetButtonDown("Jump");
       bool dashInput = Input.GetButton("Dash");

       characterMovement.moveCharacter(hMovementInput,vMovementInput,jumpInput,dashInput);
      
        }
        private void  rotation(){
          //Rotation
          float hRotationInput = Input.GetAxis("Mouse X");
          float vRotationInput = Input.GetAxis("Mouse Y");

          mouseLook.handleRotation(hRotationInput,vRotationInput);

         } 
}
